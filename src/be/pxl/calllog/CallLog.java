package be.pxl.calllog;

import java.sql.Time;
import java.time.LocalDate;
import java.util.Date;


public class CallLog {
	private String naam;	
	private Date datum;
	private Time tijd;
	private String bedrijf;
	private String omschrijving;
	private int prioriteit;
	private Status status;
	
	
	
	public CallLog(String naam, int dag, int maand, int jaar, Time tijd, String bedrijf, String omschrijving, int prioriteit,
			String status) {
		super();
		this.naam = naam;
		this.datum = new Date(dag, maand, jaar);
		this.tijd = tijd;
		this.bedrijf = bedrijf;
		this.omschrijving = omschrijving;
		this.prioriteit = prioriteit;
		
		switch(status){
		case "IGNORE": this.status = Status.IGNORE;
		break;
		case "IN PROGRESS": this.status = Status.INPROGRESS;
		break;
		case "CLOSED": this.status = Status.CLOSED;
		break;
		case "OPEN": this.status = Status.OPEN;
		break;
		default: this.status = Status.CLOSED;
		
		}
		
	}
	
}



